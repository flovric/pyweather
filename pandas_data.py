import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime
from datetime import timedelta


def p_data(response_json):
    figure_dict = {}
    def daily():
        figures_daily = []
        df = pd.DataFrame(response_json['daily'], index=response_json['daily']['time'])
        date_first = datetime.datetime.strptime(df['time'][0], '%Y-%m-%d')
        date_last = datetime.datetime.strptime(df['time'][len(df['time'])-1], '%Y-%m-%d')+timedelta(weeks=1)
        for i in range(1,len(df.columns)):
            try:
                unit = str(response_json['daily_units'][df.columns[i]])

                weights = []
                for x in range(1,8):
                    weights.append(np.around(((x*(x+1))/2)))
                multi = len(df[df.columns[i]])//7
                
                wma = []
                pred = []
                w = 0

                for j in range(len(df[df.columns[i]])):
                    w = j//7
                    w = w*7
                    wma.append(((df[df.columns[i]][j]*(weights[j]))+np.sum(np.dot(wma[w:j],weights[w:j])))/np.sum(weights[w:j+1]))

                for j in range(0,7):
                    mhm = (wma[-1]+wma[j+(len(df[df.columns[i]])%7)])/2
                    pred.append(mhm)
                    wma.append(mhm)
                
                x_values = pd.date_range(start=date_first,end=date_last)
                x_values = x_values.format(formatter=lambda x: x.strftime('%Y-%m-%d'))

                colj = df[df.columns[i]].tolist()
                colj = colj+pred
                fig, ax = plt.subplots()
                ax.bar(range(1,len(wma)+1), colj, color='C0', label=unit)
                ax.bar_label(ax.bar(range(1,len(wma)+1), np.around(colj, 1)))
                ax.plot(x_values, np.around(wma, 1), 'bo-', color='C1', label='average')
                plt.xticks(rotation=70)
                ax.set_title(df.columns[i])
                ax.legend()
                figures_daily.append(fig)
            except:
                pass
        return figures_daily

    def hourly():
        figures_hourly = []
        df = pd.DataFrame(response_json['hourly'], index=response_json['hourly']['time'])

        date_first = datetime.datetime.strptime(df['time'][0], '%Y-%m-%dT%H:%M')
        date_last = datetime.datetime.strptime(df['time'][len(df['time'])-1], '%Y-%m-%dT%H:%M')+timedelta(days=1)

        
        for i in range(1,len(df.columns)):
            try:
                unit = str(response_json['hourly_units'][df.columns[i]])
                weights = []
                for x in range(1,25):
                    weights.append(np.around(((x*(x+1))/2)))
                weights = weights*50
                
                wma = []
                pred = []
                w = 0

                for j in range(len(df[df.columns[i]])):
                    w = j//24
                    w = w*24
                    wma.append(((df[df.columns[i]][j]*(weights[j]))+np.sum(np.dot(wma[w:j],weights[w:j])))/np.sum(weights[w:j+1]))

                for j in range(0,24):
                    mhm = (wma[-1]+wma[j+(len(df[df.columns[i]])%24)])/2
                    pred.append(mhm)
                    wma.append(mhm)
                
                x_values = pd.date_range(start=date_first,end=date_last,freq='H').strftime('%Y-%m-%d %H:%M')

                colj = df[df.columns[i]].tolist()
                colj = colj+pred
                fig, ax = plt.subplots()
                ax.bar(range(1,len(wma)+1), colj, color='C0', label=unit)
                ax.bar_label(ax.bar(range(1,len(wma)+1), np.around(colj, 1)))
                ax.plot(x_values, np.around(wma, 1), 'bo-', color='C1', label='average')
                plt.xticks(rotation=70)
                ax.set_title(df.columns[i])
                ax.legend()
                figures_hourly.append(fig)
            except:
                pass
        return figures_hourly
    try:
        figure_dict['daily'] = daily()
    except:
        pass
    try:
        figure_dict['hourly'] = hourly()
    except:
        pass
    return figure_dict