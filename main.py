from gui import app
from open_meteo import response_forecast
from pandas_data import p_data

def open_meteo_arguments():
    daily_args = []
    hourly_args = []

    if app.sidechecks.daily.get() == True:
        for i in app.sidechecks.daily_dict:
            if app.sidechecks.daily_dict[i].get() == True:
                daily_args.append(i)

    if app.sidechecks.hourly.get() == True:
        for i in app.sidechecks.hourly_dict:
            if app.sidechecks.hourly_dict[i].get() == True:
                hourly_args.append(i)
    
    open_meteo_response = response_forecast(float(app.latitude_entry.get()), float(app.longitude_entry.get()), daily=daily_args, hourly=hourly_args, current_weather=app.sidechecks.current_weather.get(), start_date=app.sidechecks.start_date_entry.get(), end_date=app.sidechecks.end_date_entry.get())
    p_data(open_meteo_response[1])
    app.response_display(open_meteo_response[0], open_meteo_response[1], p_data(open_meteo_response[1]))
    

app.request_button.configure(command=open_meteo_arguments)

app.mainloop()