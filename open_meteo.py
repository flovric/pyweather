import requests
import json
from urllib.parse import urlparse

def response_forecast(response_latitude, response_longitude, **kwargs):

    url = ''
    url_base = 'https://api.open-meteo.com/v1/forecast?latitude=%s&longitude=%s&' % (response_latitude,response_longitude)

    for i in list(kwargs.keys()):
        if i == 'daily' or i == 'hourly':
            if len(kwargs[i]) > 0:
                url += str(i)+'='
                for j in kwargs[i]:
                    url += str(j)+','
                url = url.rstrip(url[-1])
        if i == 'current_weather':
            if kwargs[i] == True:
                url += str(i)+'='+'true'
        if i == 'start_date' or i == 'end_date':
            if len(kwargs[i]) == 10:
                url += str(i)+'='+kwargs[i]
        url += '&'
    url += 'timezone=auto'
    url = url_base+url
    
    response = requests.get(url)

    return response, response.json()

#Aggregations are a simple 24 hour aggregation from hourly values. The parameter &daily= accepts the following values:
response_daily = ['temperature_2m_max','temperature_2m_min','apparent_temperature_max','apparent_temperature_min','precipitation_sum','rain_sum','showers_sum','snowfall_sum','precipitation_hours','weathercode','sunrise','sunset','windspeed_10m_max','windgusts_10m_max','winddirection_10m_dominant','shortwave_radiation_sum','et0_fao_evapotranspiration']

#The parameter &hourly= accepts the following values. Most weather variables are given as an instantaneous value for the indicated hour. Some variables like precipitation are calculated from the preceding hour as an average or sum.
response_hourly = ['temperature_2m','relativehumidity_2m','dewpoint_2m','apparent_temperature','pressure_msl','surface_pressure','cloudcover','cloudcover_low','cloudcover_mid','cloudcover_high','windspeed_10m','winddirection_10m','windgusts_10m','shortwave_radiation','direct_radiation','diffuse_radiation','vapor_pressure_deficit','evapotranspiration','et0_fao_evapotranspiration','precipitation','snowfall','rain','showers','weathercode','snow_depth','freezinglevel_height','visibility','soil_temperature_0cm','soil_moisture_0_1cm']

#print(response_forecast(45.81,15.98,current_weather = True,daily=[response_daily[1],response_daily[2]],start_date='2022-06-08',end_date='2022-12-01'))

#response = requests.get(response_forecast(45.81,15.98,current_weather = True,daily=[response_daily[1],response_daily[2]],start_date='2022-06-08',end_date='2022-12-01'))
#j = response.json()
#print(j)